fun main() {
    print("Numarul de cuvinte: ")
    val numar = readLine() as String

    /**
     * 1. Scrieti o functie care sa genereze un String random (doar caractere lower-case: a-z) de dimensiuni random
     * (intre 5 si 10 caractere inclusiv)
     * ex: generateString() => "agyhiu"
     *
     * 2. Generati n (citit de la tastatura) stringuri si salvati aceste string-uri intr-o colectie
     *
     * 3. Creati un extension function care sa afiseze fiecare string din colectie, alaturi de lungimea acestuia ("dsd - 3")
     *
     * 4. Creati un extension function care sa afiseze primul string din lista, concatenat cu ultimul
     *
     * 5. Creati un extension function care sa afiseze fiecare element din lista transformat in uppercase
     *
     *
     */
}